package id.branditya.ch8challengebinar

import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import id.branditya.ch8challengebinar.helper.toDate
import id.branditya.ch8challengebinar.model.ResultPopularMovie

@ExperimentalCoilApi
@Composable
fun ItemMovie(
    resultPopularMovie: ResultPopularMovie,
    index: Int,
    onClick: (Int) -> Unit
) {
    val context = LocalContext.current
    val movieId = resultPopularMovie.id
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
            .clickable {
                onClick(index)
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("KEY_ID", movieId)
                context.startActivity(intent)
            },
        elevation = 2.dp,
        backgroundColor = Color.White,
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    ) {
        Row {
            val posterLink = resultPopularMovie.posterPath
            Image(
                painter = rememberImagePainter(
                    data = "https://image.tmdb.org/t/p/original$posterLink",
                    builder = {
                        placeholder(R.drawable.ic_launcher_background)
                    }
                ),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .padding(8.dp)
                    .size(84.dp)
                    .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
            )
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            ) {
                Text(text = resultPopularMovie.title, style = typography.h6)
                Text(text = resultPopularMovie.releaseDate.toDate()!!, style = typography.caption)
            }

        }
    }
}