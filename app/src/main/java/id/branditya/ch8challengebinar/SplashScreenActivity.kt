package id.branditya.ch8challengebinar

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import coil.annotation.ExperimentalCoilApi
import id.branditya.ch8challengebinar.ui.theme.Ch8ChallengeBinarTheme
import id.branditya.ch8challengebinar.viewmodel.SplashScreenViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoilApi
@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : ComponentActivity() {
    private val splashScreenViewModel: SplashScreenViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Ch8ChallengeBinarTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.onBackground
                ) {
                    observeLoginStatus()
                    LayoutSplash()
                }
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun LayoutSplash() {
        Ch8ChallengeBinarTheme {
            ConstraintLayout(
                modifier = Modifier.fillMaxSize()
            ) {
                val (image) = createRefs()
                Image(
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = null,
                    modifier = Modifier.constrainAs(image) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                )
            }

        }
    }

    private fun observeLoginStatus() {
        splashScreenViewModel.getLoginStatusPref().observe(this) {
            val isLogin = it
            Handler(Looper.myLooper()!!).postDelayed({
                if (isLogin) {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 3000)
        }
    }
}

