package id.branditya.ch8challengebinar

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import id.branditya.ch8challengebinar.ui.theme.Ch8ChallengeBinarTheme
import id.branditya.ch8challengebinar.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : ComponentActivity() {
    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeLoginAction()
        setContent {
            Ch8ChallengeBinarTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    LoginLayout()
                }
            }
        }
    }

    @Composable
    fun LoginLayout() {
        Ch8ChallengeBinarTheme {
            ConstraintLayout {
                val (title, imageLogo, inputEmail, inputPassword, loginButton, loginToRegister) = createRefs()

                Text(
                    text = "Login",
                    modifier = Modifier.constrainAs(title) {
                        top.linkTo(parent.top, margin = 12.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                    fontSize = 24.sp
                )

                Image(
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = "Logo",
                    modifier = Modifier
                        .constrainAs(imageLogo) {
                            top.linkTo(title.bottom, margin = 12.dp)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                val email = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = email.value,
                    onValueChange = { email.value = it },
                    singleLine = true,
                    label = { Text(text = "Email") },
                    modifier = Modifier.constrainAs(inputEmail) {
                        top.linkTo(imageLogo.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                val password = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = password.value,
                    onValueChange = { password.value = it },
                    singleLine = true,
                    label = { Text(text = "Password") },
                    modifier = Modifier.constrainAs(inputPassword) {
                        top.linkTo(inputEmail.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                val context = LocalContext.current
                Button(
                    onClick = {
                        loginViewModel.loginAction(email.value, password.value)
                    },
                    modifier = Modifier.constrainAs(loginButton) {
                        top.linkTo(inputPassword.bottom, margin = 12.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }) {
                    Text(text = "Login")
                }

                ClickableText(
                    text = AnnotatedString("Belum Punya Akun?"),
                    onClick = {
                        val intent = Intent(context, RegisterActivity::class.java)
                        context.startActivity(intent)
                    },
                    modifier = Modifier.constrainAs(loginToRegister) {
                        top.linkTo(loginButton.bottom, margin = 12.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    })
            }
        }
    }

    private fun observeLoginAction() {
        loginViewModel.accountRegistered.observe(this) {
            if (it) {
                loginToHomeActivity()
            } else {
                Toast.makeText(this, "Email atau Password Tidak Sesuai", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun loginToHomeActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}



