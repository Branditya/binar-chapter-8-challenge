package id.branditya.ch8challengebinar.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch8challengebinar.helper.MovieRepo
import id.branditya.ch8challengebinar.model.DetailMovie
import kotlinx.coroutines.launch

class DetailMovieViewModel(private val movieRepo: MovieRepo) :
    ViewModel() {
    private var _detailMovie = MutableLiveData<DetailMovie>()
    val detailMovie: LiveData<DetailMovie> get() = _detailMovie
    private var errorMessage: String by mutableStateOf("")

    fun getDataDetailMovieFromNetwork(movieId: Int) {
        viewModelScope.launch {
            try {
                val result = movieRepo.getDataDetailMovieFromNetwork(movieId)
                _detailMovie.postValue(result)
            } catch (e: Exception) {
                errorMessage = e.message.toString()
            }
        }
    }
}

