package id.branditya.ch8challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.branditya.ch8challengebinar.helper.AccountDataStoreManager

class SplashScreenViewModel(private val pref: AccountDataStoreManager) : ViewModel() {
    fun getLoginStatusPref(): LiveData<Boolean> {
        return pref.getLoginStatus().asLiveData()
    }
}