package id.branditya.ch8challengebinar.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import id.branditya.ch8challengebinar.helper.AccountDataStoreManager
import id.branditya.ch8challengebinar.helper.MovieRepo
import id.branditya.ch8challengebinar.model.ResultPopularMovie
import kotlinx.coroutines.launch

class HomeViewModel(private val pref: AccountDataStoreManager, private val movieRepo: MovieRepo) :
    ViewModel() {

    fun getDataUsername(): LiveData<String> {
        return pref.getDataUsername().asLiveData()
    }

    fun getDataAccountId(): LiveData<Int> {
        return pref.getDataAccountId().asLiveData()
    }


    var listMovie: List<ResultPopularMovie> by mutableStateOf(listOf())
    private var errorMessage: String by mutableStateOf("")

    fun getDataListMovieFromNetwork() {
        viewModelScope.launch {
            try {
                val result = movieRepo.getDataListMovieFromNetwork()
                listMovie = result.resultPopularMovies
            } catch (e: Exception) {
                errorMessage = e.message.toString()
            }
        }
    }

    fun clearDataPrefAccount() {
        viewModelScope.launch {
            pref.clearDataPrefAccount()
        }
    }

}