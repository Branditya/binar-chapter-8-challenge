package id.branditya.ch8challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch8challengebinar.database.Account
import id.branditya.ch8challengebinar.helper.AccountRepo
import kotlinx.coroutines.launch

class RegisterViewModel(private val accountRepo: AccountRepo) : ViewModel() {
    private var _accountRegistered = MutableLiveData<Boolean>()

    private var _accountAdded = MutableLiveData<Boolean>()
    val accountAdded: LiveData<Boolean> get() = _accountAdded

    fun saveToDb(username: String, email: String, password: String) {
        val account = Account(null, username, email, password, username, "", "", null)
        viewModelScope.launch {
            val result = accountRepo.insertAccount(account)
            if (result != 0L) {
                _accountAdded.postValue(true)
            }
        }
    }

}