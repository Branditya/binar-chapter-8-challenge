package id.branditya.ch8challengebinar.viewmodel

import androidx.lifecycle.*
import id.branditya.ch8challengebinar.helper.AccountDataStoreManager
import id.branditya.ch8challengebinar.helper.AccountRepo
import kotlinx.coroutines.launch

class LoginViewModel(
    private val pref: AccountDataStoreManager,
    private val accountRepo: AccountRepo
) : ViewModel() {

    private var _accountRegistered = MutableLiveData<Boolean>()
    val accountRegistered: LiveData<Boolean> get() = _accountRegistered

    private fun setPref(accountIdPref: Int, usernamePref: String, emailPref: String) {
        viewModelScope.launch {
            pref.setDataPrefAccount(accountIdPref, usernamePref, emailPref)
            _accountRegistered.postValue(true)
        }
    }

    fun loginAction(email: String?, password: String?) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountForLogin(email, password)
            if (!result.isNullOrEmpty()) {
                val accountIdPref = result[0].id!!
                val usernamePref = result[0].username
                val emailPref = result[0].email
                setPref(accountIdPref, usernamePref, emailPref)
            } else if (email != null && password != null) {
                _accountRegistered.postValue(false)
            }
        }
    }

}