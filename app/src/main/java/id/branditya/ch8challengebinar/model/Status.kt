package id.branditya.ch8challengebinar.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
