package id.branditya.ch8challengebinar.model


import com.google.gson.annotations.SerializedName

data class DetailMovieProductionCompany(
    @SerializedName("id")
    val id: Int,
    @SerializedName("logo_path")
    val logoPath: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("origin_country")
    val originCountry: String
)