package id.branditya.ch8challengebinar

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import id.branditya.ch8challengebinar.ui.theme.Ch8ChallengeBinarTheme
import id.branditya.ch8challengebinar.viewmodel.RegisterViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterActivity : ComponentActivity() {
    private val registerViewModel: RegisterViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeRegisterAction()
        setContent {
            Ch8ChallengeBinarTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    RegisterLayout()
                }
            }
        }
    }

    @Composable
    private fun RegisterLayout() {
        Ch8ChallengeBinarTheme {
            ConstraintLayout {
                val (title, imageLogo, inputUsername, inputEmail, inputPassword, inputPasswordConfirmation, loginButton) = createRefs()

                Text(
                    text = "Register",
                    modifier = Modifier.constrainAs(title) {
                        top.linkTo(parent.top, margin = 12.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                    fontSize = 24.sp
                )

                Image(
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = "Logo",
                    modifier = Modifier
                        .constrainAs(imageLogo) {
                            top.linkTo(title.bottom, margin = 12.dp)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                val username = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = username.value,
                    onValueChange = { username.value = it },
                    singleLine = true,
                    label = { Text(text = "Username") },
                    modifier = Modifier.constrainAs(inputUsername) {
                        top.linkTo(imageLogo.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                val email = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = email.value,
                    onValueChange = { email.value = it },
                    singleLine = true,
                    label = { Text(text = "Email") },
                    modifier = Modifier.constrainAs(inputEmail) {
                        top.linkTo(inputUsername.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                val password = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = password.value,
                    onValueChange = { password.value = it },
                    singleLine = true,
                    label = { Text(text = "Password") },
                    modifier = Modifier.constrainAs(inputPassword) {
                        top.linkTo(inputEmail.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                val passwordConfirm = remember { mutableStateOf("") }
                OutlinedTextField(
                    value = passwordConfirm.value,
                    onValueChange = { passwordConfirm.value = it },
                    singleLine = true,
                    label = { Text(text = "Password Confirm") },
                    modifier = Modifier.constrainAs(inputPasswordConfirmation) {
                        top.linkTo(inputPassword.bottom, margin = 12.dp)
                        start.linkTo(parent.start, margin = 12.dp)
                        end.linkTo(parent.end, margin = 12.dp)
                    })

                Button(
                    onClick = {
                        if (password.value != passwordConfirm.value) {
                            createToast("Konfirmasi Password Tidak Sesuai").show()
                        }
                        registerViewModel.saveToDb(username.value, email.value, password.value)
                    },
                    modifier = Modifier.constrainAs(loginButton) {
                        top.linkTo(inputPasswordConfirmation.bottom, margin = 12.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }) {
                    Text(text = "Register")
                }
            }
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    private fun observeRegisterAction() {
        registerViewModel.accountAdded.observe(this) {
            if (it) {
                registerToLoginActivity()
                createToast("Register Berhasil").show()
            } else {
                createToast("Register Gagal").show()
            }
        }
    }

    private fun registerToLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        finish()
        startActivity(intent)
    }
}


