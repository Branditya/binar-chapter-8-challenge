package id.branditya.ch8challengebinar.service

import id.branditya.ch8challengebinar.model.DetailMovie
import id.branditya.ch8challengebinar.model.PopularMovie
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    suspend fun getPopularMovie(@Query("api_key") key: String): PopularMovie

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") id: Int,
        @Query("api_key") key: String
    ): DetailMovie
}