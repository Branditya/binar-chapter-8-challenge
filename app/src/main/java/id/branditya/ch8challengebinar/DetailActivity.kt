package id.branditya.ch8challengebinar

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import id.branditya.ch8challengebinar.ui.theme.Ch8ChallengeBinarTheme
import id.branditya.ch8challengebinar.viewmodel.DetailMovieViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoilApi
class DetailActivity : ComponentActivity() {
    private val detailMovieViewModel: DetailMovieViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Ch8ChallengeBinarTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val movieId = intent.getIntExtra("KEY_ID", 0)
                    detailMovieViewModel.getDataDetailMovieFromNetwork(movieId)
                    DetailLayout()
                }
            }
        }
    }

    @Composable
    fun DetailLayout() {
        Ch8ChallengeBinarTheme {
            val detailMovie = detailMovieViewModel.detailMovie.observeAsState()
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                val (image, title, description) = createRefs()

                if (detailMovie.value != null) {
                    Text(
                        text = detailMovie.value!!.title,
                        modifier = Modifier.constrainAs(title) {
                            top.linkTo(parent.top, margin = 12.dp)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                    )

                    val posterLink = detailMovie.value!!.posterPath
                    Image(painter = rememberImagePainter(
                        data = "https://image.tmdb.org/t/p/original$posterLink",
                        builder = {
                            placeholder(R.drawable.ic_launcher_background)
                        }),
                        contentDescription = null,
                        modifier = Modifier
                            .constrainAs(image) {
                                top.linkTo(title.bottom, margin = 12.dp)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .width(240.dp)
                            .height(240.dp)
                    )

                    Text(
                        text = detailMovie.value!!.overview,
                        modifier = Modifier.constrainAs(description) {
                            top.linkTo(image.bottom, margin = 12.dp)
                            start.linkTo(parent.start, margin = 12.dp)
                            end.linkTo(parent.end, margin = 12.dp)
                        }
                    )
                }
            }
        }
    }
}

