package id.branditya.ch8challengebinar

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import coil.annotation.ExperimentalCoilApi
import id.branditya.ch8challengebinar.ui.theme.Ch8ChallengeBinarTheme
import id.branditya.ch8challengebinar.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoilApi
class HomeActivity : ComponentActivity() {
    private val homeViewModel: HomeViewModel by viewModel()

    var username = ""
    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeData()
        setContent {
            Ch8ChallengeBinarTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    homeViewModel.getDataListMovieFromNetwork()
                    HomeLayout()
                }
            }
        }
    }

    @ExperimentalCoilApi
    @Composable
    fun HomeLayout() {
        Ch8ChallengeBinarTheme {
            val listMovie = homeViewModel.listMovie
            Column {
                ConstraintLayout(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                ) {
                    val (topBarWelcome, buttonLogout) = createRefs()
                    val context = LocalContext.current

                    Text(
                        text = "Welcome, $username",
                        modifier = Modifier
                            .constrainAs(topBarWelcome) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start, margin = 12.dp)
                            }
                            .padding(12.dp)
                    )
                    ClickableText(
                        text = AnnotatedString("Logout"),
                        onClick = {
                            homeViewModel.clearDataPrefAccount()
                            val intent = Intent(context, LoginActivity::class.java)
                            context.startActivity(intent)
                            finish()
                        },
                        modifier = Modifier
                            .constrainAs(buttonLogout) {
                                top.linkTo(topBarWelcome.top)
                                bottom.linkTo(topBarWelcome.bottom)
                                end.linkTo(parent.end, margin = 12.dp)
                            }
                    )

                }

                Text(
                    text = "Home",
                    modifier = Modifier.align(alignment = Alignment.CenterHorizontally)
                )

                LazyColumn {
                    itemsIndexed(items = listMovie) { index, item ->
                        ItemMovie(resultPopularMovie = item, index) {
                        }
                    }
                }
            }
        }

    }

    private fun observeData() {
        homeViewModel.getDataUsername().observe(this) {
            username = it
        }
        homeViewModel.getDataAccountId().observe(this) {
            id = it
        }
    }
}