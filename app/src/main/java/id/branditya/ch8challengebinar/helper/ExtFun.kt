package id.branditya.ch8challengebinar.helper

import java.text.SimpleDateFormat
import java.util.*


fun String.toDate(): String? {
    if (this.isEmpty()) {
        return "-"
    }
    // pattern tanggal dari api
    val inputPattern = "yyyy-MM-dd"
    // pattern yang kita inginkan
    val outputPattern = "MMM dd, yyyy"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale("in"))

    // Parsing tanggal dari api, this itu adalah String yang di pake buat manggil toDate()
    // parsing adalah ubah tipe data string menjadi tipe data Date
    val inputDate = inputFormat.parse(this)

    // .format adalah ubah tipe data Date menjadi tipe data String
    return inputDate?.let {
        outputFormat.format(it)
    }

}


