package id.branditya.ch8challengebinar.helper

import android.content.Context
import id.branditya.ch8challengebinar.database.Account
import id.branditya.ch8challengebinar.database.AccountDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepo(context: Context) {
    private val mDb = AccountDatabase.getInstance(context)

    suspend fun getRegisteredAccountForLogin(email: String?, password: String?) =
        withContext(Dispatchers.IO) {
            mDb?.accountDao()?.getRegisteredAccountForLogin(email, password)
        }

    suspend fun getRegisteredAccountWithEmail(email: String?) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.getRegisteredAccountWithEmail(email)
    }

    suspend fun insertAccount(account: Account) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.insertAccount(account)
    }

}