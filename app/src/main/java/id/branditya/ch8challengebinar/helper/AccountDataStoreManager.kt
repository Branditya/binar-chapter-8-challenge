package id.branditya.ch8challengebinar.helper

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AccountDataStoreManager(private val context: Context) {
    suspend fun setDataPrefAccount(accountId: Int, username: String, email: String) {
        context.dataStore.edit { pref ->
            pref[IS_LOGIN_KEY] = true
            pref[ACCOUNT_ID_KEY] = accountId
            pref[USERNAME_KEY] = username
            pref[EMAIL_KEY] = email
        }
    }

    fun getLoginStatus(): Flow<Boolean> {
        return context.dataStore.data.map { pref ->
            pref[IS_LOGIN_KEY] ?: false
        }
    }

    fun getDataUsername(): Flow<String> {
        return context.dataStore.data.map { pref ->
            pref[USERNAME_KEY] ?: ""
        }
    }

    fun getDataEmail(): Flow<String> {
        return context.dataStore.data.map { pref ->
            pref[EMAIL_KEY] ?: ""
        }
    }

    fun getDataAccountId(): Flow<Int> {
        return context.dataStore.data.map { pref ->
            pref[ACCOUNT_ID_KEY] ?: 0
        }
    }

    suspend fun clearDataPrefAccount() {
        context.dataStore.edit {
            it.clear()
        }
    }

    companion object {
        private const val DATA_STORE_NAME = "account_preference"
        private val IS_LOGIN_KEY = booleanPreferencesKey("is_login_key")
        private val ACCOUNT_ID_KEY = intPreferencesKey("account_id_key")
        private val USERNAME_KEY = stringPreferencesKey("username_key")
        private val EMAIL_KEY = stringPreferencesKey("email_key")
        private val Context.dataStore by preferencesDataStore(name = DATA_STORE_NAME)
    }
}