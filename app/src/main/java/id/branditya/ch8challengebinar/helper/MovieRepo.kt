package id.branditya.ch8challengebinar.helper

import id.branditya.ch8challengebinar.BuildConfig
import id.branditya.ch8challengebinar.service.ApiService

class MovieRepo(private val apiService: ApiService) {

    suspend fun getDataDetailMovieFromNetwork(movieId: Int) =
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY)

    suspend fun getDataListMovieFromNetwork() = apiService.getPopularMovie(BuildConfig.API_KEY)
}