package id.branditya.ch8challengebinar.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface AccountDao {
    @Query("SELECT * FROM Account")
    fun getAllAccount(): List<Account>

    @Query("SELECT * FROM Account WHERE email = :email AND password = :password")
    fun getRegisteredAccountForLogin(email: String?, password: String?): List<Account>

    @Query("SELECT * FROM Account WHERE email = :email")
    fun getRegisteredAccountWithEmail(email: String?): List<Account>

    @Query("SELECT * FROM Account WHERE id = :id")
    fun getRegisteredAccountWithId(id: Int): List<Account>

    @Query("UPDATE Account SET profileImage = :profileImage WHERE id = :id")
    fun updateProfileImage(id: Int, profileImage: String)

    @Query(
        "UPDATE Account SET username = :username, " +
                "fullname = :fullname, " +
                "birthdate = :birthdate, " +
                "address = :address WHERE id = :id"
    )
    fun updateProfileAccount(
        id: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ): Int

    @Insert(onConflict = REPLACE)
    fun insertAccount(account: Account): Long

    @Update
    fun updateAccount(account: Account): Int

    @Delete
    fun deleteAccount(account: Account): Int
}