package id.branditya.ch8challengebinar

import android.app.Application
import id.branditya.ch8challengebinar.helper.AccountDataStoreManager
import id.branditya.ch8challengebinar.helper.AccountRepo
import id.branditya.ch8challengebinar.helper.MovieRepo
import id.branditya.ch8challengebinar.service.ApiClient
import id.branditya.ch8challengebinar.viewmodel.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, repoModule, viewModelModule))
        }
    }

    private val appModule = module {
        single { ApiClient.instance }
        single { AccountDataStoreManager(androidContext()) }
    }

    private val repoModule = module {
        single { MovieRepo(get()) }
        single { AccountRepo(androidContext()) }
    }

    private val viewModelModule = module {
        factory { RegisterViewModel(get()) }
        factory { LoginViewModel(get(), get()) }
        factory { HomeViewModel(get(), get()) }
        factory { DetailMovieViewModel(get()) }
        factory { SplashScreenViewModel(get()) }
    }
}